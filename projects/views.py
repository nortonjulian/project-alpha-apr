from django.shortcuts import render, redirect, get_object_or_404
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm


@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "project_list": projects,
    }
    return render(request, "projects/list.html", context)


def redirect_to_project_list(request):
    return redirect("list_projects")


# @login_required
# def show_project(request, id):
#     project = get_object_or_404(Project, id=id)
#     context = {"project_list": project}
#     return render(request, "projects/detail.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {"project": project}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.author = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)


# ERROR: test_projects_list_shows_no_member_projects_when_none_exist (tests.test_feature_08.FeatureTests)
# ERROR: test_projects_list_shows_no_projects_when_member_of_one (tests.test_feature_08.FeatureTests)
# django.urls.exceptions.NoReverseMatch: Reverse for 'show_project' with no arguments not found. 1 pattern(s) tried: ['projects/(?P<id>[0-9]+)/\\Z']
